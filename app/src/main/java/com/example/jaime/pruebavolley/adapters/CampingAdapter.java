package com.example.jaime.pruebavolley.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jaime.pruebavolley.R;
import com.example.jaime.pruebavolley.models.Camping;

import java.util.List;

public class CampingAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Camping> list;

    public CampingAdapter(Context context, int layout, List<Camping> list) {
        this.context = context;
        this.layout = layout;
        this.list = list;
    }

    @Override
    public int getCount() {
       return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder vH;
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(layout,null);
            vH = new ViewHolder();
            vH.id = (TextView) convertView.findViewById(R.id.txt_id);
            vH.nombre = (TextView) convertView.findViewById(R.id.txt_nombre);
            vH.direccion = (TextView) convertView.findViewById(R.id.txt_direccion);
            convertView.setTag(vH);
        }else{
            vH = (ViewHolder) convertView.getTag();
        }

        final Camping currentCamping = (Camping) getItem(position);
        vH.id.setText(String.valueOf(currentCamping.getId()));
        vH.nombre.setText(currentCamping.getNombre());
        vH.direccion.setText(currentCamping.getDireccion());

        return convertView;
    }

    private static class ViewHolder{
        private TextView nombre,direccion,id;
    }
}
