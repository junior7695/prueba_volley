package com.example.jaime.pruebavolley.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.example.jaime.pruebavolley.R;
import com.example.jaime.pruebavolley.adapters.CampingAdapter;
import com.example.jaime.pruebavolley.models.Camping;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    CampingAdapter campingAdapter;
    List<Camping> campings;
    String PLACES_URL = "http://nexo.carm.es/nexo/archivos/recursos/opendata/json/Campings.json";
    String LOG_TAG = "VolleyPlacesRemoteDS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        campings = getAllCampings();
        listView = (ListView) findViewById(R.id.listView);
        campingAdapter = new CampingAdapter(this,R.layout.list_view_item,campings);
        listView.setAdapter(campingAdapter);
    }

    public List<Camping> getAllCampings(){
        final List<Camping> list = new ArrayList<>();

        // Instancia de Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        // Listo para el Request
        JsonArrayRequest request = new JsonArrayRequest(
                Request.Method.GET,
                PLACES_URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        // Respuesta OK
                        Log.d(LOG_TAG, response.toString());
                        try {
                            for (int i = 0; i < response.length(); i++){
                                JSONObject jsonObject = response.getJSONObject(i);
                                list.add(new Camping(
                                        jsonObject.getInt("Código"),
                                        jsonObject.getString("Nombre"),
                                        jsonObject.getString("Dirección")
                                ));
                                campingAdapter.notifyDataSetChanged();
                            }

                            //txt.setText(nombre);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Error en respuesta
                Log.d(LOG_TAG,error.toString());
                //txt.setText(error.toString());
            }
        }
        );

        //Enviar Request
        requestQueue.add(request);



        return list;
    }
}
